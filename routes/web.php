<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['prefix' => '/', 'middleware' => 'auth'], function() {
    Route::get('/', 'HomeController@index');

    Route::group(['prefix' => '/', 'middleware' => 'auth'], function() {
        Route::get('customers/bulk-import', 'CustomerController@download');
        Route::post('customers/bulk-import', 'CustomerController@upload');
        Route::resource('/customers', 'CustomerController');

        Route::get('storages/bulk-import', 'StorageController@download');
        Route::post('storages/bulk-import', 'StorageController@upload');
        Route::resource('/storages', 'StorageController');

        Route::get('sales/bulk-import', 'SalesController@download');
        Route::post('sales/bulk-import', 'SalesController@upload');
        Route::resource('/sales', 'SalesController');

        Route::resource('/sales-orders', 'SalesOrderController');
    });
});
