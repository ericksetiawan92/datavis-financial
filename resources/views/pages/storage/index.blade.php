@extends('masters.main_layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Master Storage
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-folder"></i> Home</a></li>
                <li><a href="#">Master</a></li>
                <li class="active">Storage</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))
                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                            @endif
                        @endforeach
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#active" data-toggle="tab">Active</a></li>
                            <li><a href="#deleted" data-toggle="tab">Non Active</a></li>
                        </ul>
                    </div>
                    
                    <div class="tab-content">
                        <!-- Font Awesome Icons -->
                        <div class="tab-pane active" id="active">
                            <div class="box">
                                <div class="box-header text-right">
                                    <div class="col-xs-6">
                                        <form role="form" method="POST" action="{{ url('storages/bulk-import') }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group col-xs-5">
                                                <input type="file" name="file" class="form-control" required>
                                            </div>
                                            <div class="form-group col-xs-2">
                                                <button type="submit" class="btn btn-success">
                                                    <i class="fa fa-angle-up"></i> Upload
                                                </button>
                                            </div>
                                            <div class="form-group col-xs-5">
                                                <a class="btn btn-success" href="{{ url('storages/bulk-import') }}">
                                                    <i class="fa fa-file"></i> Download template bulk import
                                                </a>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="col-xs-6">
                                        <a class="btn btn-primary" href="{{ url('storages/create') }}">
                                            <i class="fa fa-plus"></i> Add new record
                                        </a>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="example2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Storage ID</th>
                                                <th>Name</th>
                                                <th>Small Denomination</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($storages as $storage)
                                                <tr>
                                                    <td>{{ $storage->storage_id }}</td>
                                                    <td>{{ $storage->name }}</td>
                                                    <td>{{ $storage->small_denomination }}</td>
                                                    <td>
                                                        <a href="{{ url('storages/' . $storage->id . '/edit' )}}" class="btn btn-warning btn-sm">Edit</a>
                                                        <button type="button" onClick="deleteRecord({{$storage->id}}, '{{$storage->name}}')" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-delete">
                                                            Delete
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>

                        <div class="tab-pane" id="deleted">
                            <div class="tab-pane" id="deleted">
                                <div class="box">
                                    <div class="box-header text-right">
                                        <a class="btn btn-primary" href="{{ url('storages/create') }}">
                                            <i class="fa fa-plus"></i> Add new record
                                        </a>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <table id="example3" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Storage ID</th>
                                                    <th>Name</th>
                                                    <th>Small Denomination</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($deletedStorages as $storage)
                                                    <tr>
                                                        <td>{{ $storage->storage_id }}</td>
                                                        <td>{{ $storage->name }}</td>
                                                        <td>{{ $storage->small_denomination }}</td>
                                                        <td>
                                                            <a href="{{ url('storages/' . $storage->id )}}" class="btn btn-warning btn-sm">View</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="modal-delete">
        <form role="form" id="form_delete" method="POST">
        {!! method_field('delete') !!}
            @csrf
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Delete record</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure to delete <span id="name_delete" class="strong"></span> ? </p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Yes</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })

            $('#example3').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>

    <script>
        function deleteRecord(id, name) {
            $("#name_delete").html(name);

            var action = "{{url('storages')}}/" + id;
            var form = document.getElementById('form_delete');
            form.action = action ;
        }
    </script>
@stop