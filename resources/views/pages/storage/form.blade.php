@extends('masters.main_layout')

@section('content')  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Master Storage
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-folder"></i> Home</a></li>
                <li><a href="#">Master</a></li>
                <li class="active">Storage</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        @if (isset($storage))
                            <!-- form start -->
                            <form role="form" method="POST" action="{{ url('storages/' . $storage->id) }}">
                                {!! method_field('patch') !!}
                                @csrf
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Storage ID *</label>
                                        <input type="text" name="storage_id" value="{{ $storage->storage_id }}" class="form-control" placeholder="Storage id (required)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Name *</label>
                                        <input type="text" name="name" value="{{ $storage->name }}" class="form-control" placeholder="Storage name (required)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Small Denomination *</label>
                                        <input type="text" id="small_denomination" name="small_denomination" value="{{ $storage->small_denomination }}" class="form-control" placeholder="Small denomination (required)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Big Denomination</label>
                                        <div class="col-xs-12">
                                            <div class="col-xs-6">
                                                <input type="text" value="{{ $nameBigDenominations[0] }}" name="big_denomination[]" class="form-control" placeholder="Big denomination">
                                            </div>
                                            <div class="col-xs-1" style="padding-top:5px">
                                                <span>=</span>
                                            </div>
                                            <div class="col-xs-3">
                                                <input type="text" value="{{ $valueBigDenominations[0] }}" name="value_big_denomination[]" class="form-control" placeholder="Value big denomination">
                                            </div>
                                            <div class="col-xs-2" style="padding-top:5px">
                                                <span id="small_denomination_1"></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="col-xs-6">
                                                <input type="text" value="{{ $nameBigDenominations[1] }}" name="big_denomination[]" class="form-control" placeholder="Big denomination">
                                            </div>
                                            <div class="col-xs-1" style="padding-top:5px">
                                                <span>=</span>
                                            </div>
                                            <div class="col-xs-3">
                                                <input type="text" value="{{ $valueBigDenominations[1] }}" name="value_big_denomination[]" class="form-control" placeholder="Value big denomination">
                                            </div>
                                            <div class="col-xs-2" style="padding-top:5px">
                                                <span id="small_denomination_2"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->

                                <div class="box-footer text-right">
                                    <a onclick="goBack()" class="btn btn-danger">Back</a>
                                    @if (!isset($content))
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    @endif
                                </div>
                            </form>
                        @else
                            <!-- form start -->
                            <form role="form" method="POST" action="{{ url('storages') }}">
                                @csrf
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Storage ID *</label>
                                        <input type="text" name="storage_id" class="form-control" placeholder="Storage id (required)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Name *</label>
                                        <input type="text" name="name" class="form-control" placeholder="Storage name (required)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Small Denomination</label>
                                        <input type="text" id="small_denomination" name="small_denomination" class="form-control" placeholder="Small denomination">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Big Denomination</label>
                                        <div class="col-xs-12">
                                            <div class="col-xs-6">
                                                <input type="text" name="big_denomination[]" class="form-control" placeholder="Big denomination">
                                            </div>
                                            <div class="col-xs-1" style="padding-top:5px">
                                                <span>=</span>
                                            </div>
                                            <div class="col-xs-3">
                                                <input type="text" name="value_big_denomination[]" class="form-control" placeholder="Value big denomination">
                                            </div>
                                            <div class="col-xs-2" style="padding-top:5px">
                                                <span id="small_denomination_1"></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="col-xs-6">
                                                <input type="text" name="big_denomination[]" class="form-control" placeholder="Big denomination">
                                            </div>
                                            <div class="col-xs-1" style="padding-top:5px">
                                                <span>=</span>
                                            </div>
                                            <div class="col-xs-3">
                                                <input type="text" name="value_big_denomination[]" class="form-control" placeholder="Value big denomination">
                                            </div>
                                            <div class="col-xs-2" style="padding-top:5px">
                                                <span id="small_denomination_2"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->

                                <div class="box-footer text-right">
                                    <a onclick="goBack()" class="btn btn-danger">Back</a>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>           
@stop

@section('js')
    <script>
        $(document).ready(function() {
            if ($("#small_denomination").val()) {
                value = $("#small_denomination").val();
                $("#small_denomination_1").html(value);
                $("#small_denomination_2").html(value);
            }
        });

        $("#small_denomination").keyup(function() {
            value = $("#small_denomination").val();
            $("#small_denomination_1").html(value);
            $("#small_denomination_2").html(value);
        });
    </script>
@stop