@extends('masters.main_layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Sales Order
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-folder"></i> Home</a></li>
                <li><a href="#">Finance</a></li>
                <li class="active">Sales Order</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))
                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                            @endif
                        @endforeach
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#active" data-toggle="tab">Active</a></li>
                            <li><a href="#deleted" data-toggle="tab">Non Active</a></li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <!-- Font Awesome Icons -->
                        <div class="tab-pane active" id="active">
                            <div class="box">
                                <div class="box-header text-right">
                                    <div class="col-xs-6"></div>
                                    <div class="col-xs-6">
                                        <a class="btn btn-primary" href="{{ url('sales-orders/create') }}">
                                            <i class="fa fa-plus"></i> Add new record
                                        </a>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="example2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>SO Number</th>
                                                <th>PO Number</th>
                                                <th>Date</th>
                                                <th>Customer</th>
                                                <th>Sales</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($salesOrders as $so)
                                                <tr>
                                                    <td>{{ $so->so_number }}</td>
                                                    <td>{{ $so->po_number }}</td>
                                                    <td>{{ $so->date }}</td>
                                                    <td>{{ $so->customer->name }}</td>
                                                    <td>{{ $so->sales->name }}</td>
                                                    <td>
                                                        <a href="{{ url('sales-orders/' . $so->id . '/edit' )}}" class="btn btn-warning btn-sm">Edit</a>
                                                        <button type="button" onClick="deleteRecord({{$so->id}}, '{{$so->so_number}}')" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-delete">
                                                            Delete
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>

                        <div class="tab-pane" id="deleted">
                            <div class="box">
                                <div class="box-header text-right">
                                    <div class="col-xs-12">
                                        <a class="btn btn-primary" href="{{ url('sales-orders/create') }}">
                                            <i class="fa fa-plus"></i> Add new record
                                        </a>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="example3" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>SO Number</th>
                                                <th>PO Number</th>
                                                <th>Date</th>
                                                <th>Customer</th>
                                                <th>Sales</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($deletedSalesOrders as $so)
                                                <tr>
                                                    <td>{{ $so->so_number }}</td>
                                                    <td>{{ $so->po_number }}</td>
                                                    <td>{{ $so->date }}</td>
                                                    <td>{{ $so->customer->name }}</td>
                                                    <td>{{ $so->sales->name }}</td>
                                                    <td>
                                                        <a href="{{ url('sales-orders/' . $so->id )}}" class="btn btn-warning btn-sm">View</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="modal-delete">
        <form role="form" id="form_delete" method="POST">
        {!! method_field('delete') !!}
            @csrf
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Delete record</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure to delete <span id="name_delete" class="strong"></span> ? </p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Yes</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })

            $('#example3').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>

    <script>
        function deleteRecord(id, name) {
            $("#name_delete").html(name);

            var action = "{{url('sales-orders')}}/" + id;
            var form = document.getElementById('form_delete');
            form.action = action ;
        }
    </script>
@stop