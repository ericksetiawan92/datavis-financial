@extends('masters.main_layout')

@section('content')  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Sales Order
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-folder"></i> Home</a></li>
                <li><a href="#">Finance</a></li>
                <li class="active">Sales Order</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="box box-primary">
                <!-- form start -->
                <form role="form" method="POST" action="{{ url('sales-orders') }}">
                @csrf
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Purchase Order Number *</label>
                                    <input type="text" class="form-control pull-right" name="po_number" placeholder="Purchase order number.." required>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Sales Order Number *</label>
                                    <input type="text" class="form-control pull-right" name="so_number" placeholder="Sales order number.." required>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Date *</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="datepicker" name="date" required>
                                    </div>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Deadline *</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="datepicker1" name="deadline" required>
                                    </div>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Customer *</label>
                                    <select class="form-control select2" style="width: 100%;" name="customer_id" required>
                                        <option value="">Select a customer</option>
                                        @foreach ($customers as $customer)
                                            <option value="{{$customer->id}}">{{$customer->customer_id . ' - ' . $customer->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Sales *</label>
                                    <select class="form-control select2" style="width: 100%;" name="sales_id" required>
                                        <option value="">Select a sales</option>
                                        @foreach ($sales as $sale)
                                            <option value="{{$sale->id}}">{{$sale->sales_id . ' - ' . $sale->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Term</label>
                                    <select class="form-control select2" style="width: 100%;" name="term">
                                        <option value="">Select a term</option>
                                        <option value="COD">C.O.D</option>
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <br><br><br>

                        <div class="row">
                            <div class="col-md-3">
                                <a class="btn btn-primary" id="insert_item" data-toggle="modal" data-target="#modal_list_item">Insert Item</a>
                                <a class="btn btn-success" id="insert_custom_item" onclick="insertCustomItem()">Custom Item</a>
                            </div>
                            <div class="col-md-2">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="include_tax" id="include_tax"> Include Tax
                                    </label>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <div class="col-md-12">
                                <table id="list_item" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Storage ID</th>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Amount</th>
                                            <th>Denomination</th>
                                            <th>T</th>
                                            <th>Tax</th>
                                            <th>Total</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="body_list_item">

                                    </tbody>    
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6"></div>
                            <div class="col-md-3"></div>
                            <div class="col-md-3">
                                <table>
                                    <tr>
                                        <td style="width: 50px">Tax</td>
                                        <td style="width: 50px">:</td>
                                        <td id="tax_total">Rp 0</td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td>:</td>
                                        <td id="price_total">Rp 0</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer text-right">
                        <a onclick="goBack()" class="btn btn-danger">Back</a>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </section>
    </div> 
    
    <div class="modal fade" id="modal_list_item">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">List Items</h4>
                </div>
                <div class="modal-body">
                    <table id="example2" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Storage ID</th>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($items as $item)
                                <tr>
                                    <td>{{ $item->storage_id }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>
                                        <button class="btn btn-sm btn-success" onclick="insertItem('{{$item->id}}', '{{$item->storage_id}}', '{{$item->name}}', '{{$item->small_denomination}}', '{{$item->big_denomination_1}}', '{{$item->big_denomination_2}}')" >Take</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        var indexCustomItem = 1111;

        $(document).ready(function() {        
            //Data table
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })

            //Initialize Select2 Elements
            $('.select2').select2()

            //Date picker
            $('#datepicker').datepicker({autoclose: true}).datepicker("setDate", new Date());
            $('#datepicker1').datepicker({autoclose: true}).datepicker("setDate", new Date());

            //Include tax checkbox
            $("#include_tax").change(function() {
                $('[data-tag-price]').each(function () {
                    var id = $(this).attr('id');
                    var attr_id = id.split("-");

                    var index = attr_id[1];
                    calculateRecord(index);
                })
            });
        });

        function insertItem(id, storageId, name, smallDenomination, bigDenomination1, bigDenomination2)
        {
            var selectBoxDenomination = null;
            if (smallDenomination) {
                selectBoxDenomination += "<option value='"+smallDenomination+"'>"+smallDenomination+"</option>";
            }
            if (bigDenomination1) {
                selectBoxDenomination += "<option value='"+bigDenomination1+"'>"+bigDenomination1+"</option>";
            }
            if (bigDenomination2) {
                selectBoxDenomination += "<option value='"+bigDenomination2+"'>"+bigDenomination2+"</option>";
            }

            var element = 
                "<tr id='item-"+indexCustomItem+"'>"+
                    "<td>"+
                        "<input type='hidden' name='item_ids[]' value='"+id+"'>"+
                        storageId+
                    "</td>"+
                    "<td>"+
                        name+
                    "</td>"+
                    "<td>"+
                        "<input type='text' data-tag-price=[] id='price-"+indexCustomItem+"' name='item_prices[]' onkeyup='calculateRecord("+indexCustomItem+")'>"+
                    "</td>"+
                    "<td>"+
                        "<input type='text' data-tag-amount=[] id='amount-"+indexCustomItem+"' name='item_amounts[]' onkeyup='calculateRecord("+indexCustomItem+")'>"+
                    "</td>"+
                    "<td>"+
                        "<select class='form-control select2' style='width: 100%;' name='item_denominations[]'>"+
                            selectBoxDenomination+
                        "</select>"+
                    "</td>"+
                    "<td>"+
                        "<input type='text' style='width: 80px' data-tag-tax-option=[] id='tax-option-"+indexCustomItem+"' name='item_tax_options[]' onkeyup='calculateTax("+indexCustomItem+")'>"+
                    "</td>"+
                    "<td>"+
                        "<input type='text' data-tag-tax=[] id='tax-"+indexCustomItem+"' name='item_taxes[]' value='0' readonly>"+
                    "</td>"+
                    "<td>"+
                        "<input type='text' data-tag-total=[] id='total-"+indexCustomItem+"' name='item_totals[]' value='0' readonly>"+
                    "</td>"+
                    "<td>"+
                        "<a class='btn btn-danger' onclick='removeItem("+indexCustomItem+")'>Remove</a>"+
                    "</td>"+
                "</tr>";
            
            $("#body_list_item").append(element);

            indexCustomItem++;
        }

        function insertCustomItem()
        {
            var element = 
                "<tr id='item-"+indexCustomItem+"'>"+
                    "<td colspan='2'>"+
                        "<input type='text' name='item_ids[]'>"+
                    "</td>"+
                    "<td>"+
                        "<input type='text' data-tag-price=[] id='price-"+indexCustomItem+"' name='item_prices[]' onkeyup='calculateRecord("+indexCustomItem+")'>"+
                    "</td>"+
                    "<td>"+
                        "<input type='text' data-tag-amount=[] id='amount-"+indexCustomItem+"' name='item_amounts[]' onkeyup='calculateRecord("+indexCustomItem+")'>"+
                    "</td>"+
                    "<td><input type='hidden' name='item_denominations[]'>-</td>"+
                    "<td>"+
                        "<input type='text' style='width: 80px' data-tag-tax-option=[] id='tax-option-"+indexCustomItem+"' name='item_tax_options[]' onkeyup='calculateTax("+indexCustomItem+")'>"+
                    "</td>"+
                    "<td>"+
                        "<input type='text' data-tag-tax=[] id='tax-"+indexCustomItem+"' name='item_taxes[]' value='0' readonly>"+
                    "</td>"+
                    "<td>"+
                        "<input type='text' data-tag-total=[] id='total-"+indexCustomItem+"' name='item_totals[]' value='0' readonly>"+
                    "</td>"+
                    "<td>"+
                        "<a class='btn btn-danger' onclick='removeItem("+indexCustomItem+")'>Remove</a>"+
                    "</td>"+
                "</tr>";
            
            $("#body_list_item").append(element);

            indexCustomItem++;
        }

        function removeItem(id)
        {
            $("#item-"+id).remove();
            calculateRecord(id);
            calculateFinal();
        }

        function calculateRecord(id)
        {
            var price = $('#price-'+id).val();
            var amount = $('#amount-'+id).val();
            var taxOption = $('#tax-option-'+id).val();
            
            var tax = 0;
            var total = 0;
            if (taxOption) {
                if (document.getElementById("include_tax").checked) {
                    tax = Math.round((10 / 110) * (price * amount) * 100) / 100;
                    total = price * amount;
                } else {
                    tax = Math.round((10 / 100) * (price * amount) * 100) / 100;
                    total = (price * amount) + tax;
                }
            } else {
                total = price * amount;
            }

            $('#tax-'+id).val(tax);
            $('#total-'+id).val(total);
            calculateFinal();
        }

        function calculateTax(id)
        {
            calculateRecord(id);
        }

        function calculateFinal()
        {
            var total = 0;
            var tax = 0;
            $('[data-tag-total]').each(function () {
                total += Number($(this).val());
            })
            $('[data-tag-tax]').each(function () {
                tax += Number($(this).val());
            })

            $('#tax_total').html('Rp ' + tax);
            $('#price_total').html('Rp ' + total);
        }
    </script>
@stop