@extends('masters.main_layout')

@section('content')  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Master Sales
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-folder"></i> Home</a></li>
                <li><a href="#">Master</a></li>
                <li class="active">Sales</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        @if (isset($sales))
                            <!-- form start -->
                            <form role="form" method="POST" action="{{ url('sales/' . $sales->id) }}">
                                {!! method_field('patch') !!}
                                @csrf
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Sales ID *</label>
                                        <input type="text" name="sales_id" value="{{ $sales->sales_id }}" class="form-control" placeholder="Sales id (required)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Name *</label>
                                        <input type="text" name="name" value="{{ $sales->name }}" class="form-control" placeholder="Sales name (required)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Jobtitle*</label>
                                        <input type="text" name="jobtitle" value="{{ $sales->jobtitle }}" class="form-control" placeholder="Sales jobtitle" required>
                                    </div>
                                </div>
                                <!-- /.box-body -->

                                <div class="box-footer text-right">
                                    <a onclick="goBack()" class="btn btn-danger">Back</a>
                                    @if (!isset($content))
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    @endif
                                </div>
                            </form>
                        @else
                            <!-- form start -->
                            <form role="form" method="POST" action="{{ url('sales') }}">
                                @csrf
                                <!-- /.box-header -->
                                <div class="box-body">
                                <div class="form-group">
                                        <label for="exampleInputEmail1">Sales ID *</label>
                                        <input type="text" name="sales_id" class="form-control" placeholder="Sales id (required)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Name *</label>
                                        <input type="text" name="name" class="form-control" placeholder="Sales name (required)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Jobtitle*</label>
                                        <input type="text" name="jobtitle" class="form-control" placeholder="Sales jobtitle" required>
                                    </div>
                                </div>
                                <!-- /.box-body -->

                                <div class="box-footer text-right">
                                    <a onclick="goBack()" class="btn btn-danger">Back</a>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>           
@stop