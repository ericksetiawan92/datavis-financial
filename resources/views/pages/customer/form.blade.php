@extends('masters.main_layout')

@section('content')  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Master Customer
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-folder"></i> Home</a></li>
                <li><a href="#">Master</a></li>
                <li class="active">Customer</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        @if (isset($customer))
                            <!-- form start -->
                            <form role="form" method="POST" action="{{ url('customers/' . $customer->id) }}">
                                {!! method_field('patch') !!}
                                @csrf
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Customer ID *</label>
                                        <input type="text" name="customer_id" value="{{ $customer->customer_id }}" class="form-control" placeholder="Customer id (required)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Name *</label>
                                        <input type="text" name="name" value="{{ $customer->name }}" class="form-control" placeholder="Customer name (required)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Email</label>
                                        <input type="email" name="email" value="{{ $customer->email }}" class="form-control" placeholder="Customer email">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Phone Office*</label>
                                        <input type="number" name="phone_office" value="{{ $customer->phone_office }}" class="form-control" placeholder="Customer phone office (required)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Contact Person *</label>
                                        <input type="text" name="contact_person" value="{{ $customer->contact_person }}" class="form-control" placeholder="Contact person (required)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Address *</label>
                                        <input type="text" name="address" value="{{ $customer->address }}" class="form-control" placeholder="Customer address (required)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">PIC Finance</label>
                                        <input type="text" name="pic_finance" value="{{ $customer->pic_finance }}" class="form-control" placeholder="Customer PIC Finance">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Finance Phone</label>
                                        <input type="text" name="finance_phone" value="{{ $customer->finance_phone }}" class="form-control" placeholder="Customer Finance Phone">
                                    </div>
                                    <div class="form-group col-xs-4">
                                        <label for="exampleInputPassword1">City</label>
                                        <input type="text" name="city" value="{{ $customer->city }}" class="form-control" placeholder="City">
                                    </div>
                                    <div class="form-group col-xs-4">
                                        <label for="exampleInputPassword1">Province</label>
                                        <input type="text" name="province" value="{{ $customer->province }}" class="form-control" placeholder="Province">
                                    </div>
                                    <div class="form-group col-xs-4">
                                        <label for="exampleInputPassword1">Postal Code</label>
                                        <input type="number" name="postal_code" value="{{ $customer->postal_code }}" class="form-control" placeholder="Postal Code">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Website</label>
                                        <input type="text" name="website" value="{{ $customer->website }}" class="form-control" placeholder="Customer website">
                                    </div>
                                </div>
                                <!-- /.box-body -->

                                <div class="box-footer text-right">
                                    <a onclick="goBack()" class="btn btn-danger">Back</a>
                                    @if (!isset($content))
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    @endif
                                </div>
                            </form>
                        @else
                            <!-- form start -->
                            <form role="form" method="POST" action="{{ url('customers') }}">
                                @csrf
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Customer ID *</label>
                                        <input type="text" name="customer_id" class="form-control" placeholder="Customer id (required)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Name *</label>
                                        <input type="text" name="name" class="form-control" placeholder="Customer name (required)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Email</label>
                                        <input type="email" name="email" class="form-control" placeholder="Customer email">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Phone Office*</label>
                                        <input type="number" name="phone_office" class="form-control" placeholder="Customer phone office (required)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Contact Person *</label>
                                        <input type="text" name="contact_person" class="form-control" placeholder="Contact person (required)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Address *</label>
                                        <input type="text" name="address" class="form-control" placeholder="Customer address (required)" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">PIC Finance</label>
                                        <input type="text" name="pic_finance" class="form-control" placeholder="Customer PIC Finance">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Finance Phone</label>
                                        <input type="text" name="finance_phone" class="form-control" placeholder="Customer Finance Phone">
                                    </div>
                                    <div class="form-group col-xs-4">
                                        <label for="exampleInputPassword1">City</label>
                                        <input type="text" name="city" class="form-control" placeholder="City">
                                    </div>
                                    <div class="form-group col-xs-4">
                                        <label for="exampleInputPassword1">Province</label>
                                        <input type="text" name="province" class="form-control" placeholder="Province">
                                    </div>
                                    <div class="form-group col-xs-4">
                                        <label for="exampleInputPassword1">Postal Code</label>
                                        <input type="number" name="postal_code" class="form-control" placeholder="Postal Code">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Website</label>
                                        <input type="text" name="website" class="form-control" placeholder="Customer website">
                                    </div>
                                </div>
                                <!-- /.box-body -->

                                <div class="box-footer text-right">
                                    <a onclick="goBack()" class="btn btn-danger">Back</a>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>           
@stop