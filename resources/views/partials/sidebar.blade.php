<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="@if(isset($page) && in_array($page, ['customer', 'storage', 'sales'])) active @endif treeview">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Master</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/customers') }}"><i class="fa fa-circle-o"></i> Customer</a></li>
                    <li><a href="{{ url('/storages') }}"><i class="fa fa-circle-o"></i> Storage</a></li>
                    <li><a href="{{ url('/sales') }}"><i class="fa fa-circle-o"></i> Sales</a></li>
                </ul>
            </li>
            <li class="@if(isset($page) && in_array($page, ['sales-order'])) active @endif treeview">
                <a href="#">
                    <i class="fa fa-money"></i> <span>Finance</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/sales-orders') }}"><i class="fa fa-circle-o"></i> Sales Order</a></li>
                </ul>
            </li>
            <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-user text-yellow"></i><span>Master User</span></a></li>
            <li><a href="#"><i class="fa fa-key text-yellow"></i><span>Master Previledge</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>