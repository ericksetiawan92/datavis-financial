<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('pic_finance')->nullable();
            $table->string('finance_phone')->nullable();
            $table->string('phone_office')->nullable();
            $table->dropColumn('tax_address');
            $table->dropColumn('phone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('tax_address')->nullable();
            $table->string('phone')->nullable();
            $table->dropColumn('pic_finance')->nullable();
            $table->dropColumn('finance_phone');
            $table->dropColumn('phone_office');
        });
    }
}
