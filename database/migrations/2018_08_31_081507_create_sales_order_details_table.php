<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sales_order_id')->nullable()->unsigned();
            $table->integer('storage_id')->nullable()->unsigned();
            $table->string('storage_name')->nullable();
            $table->string('price')->default(0);
            $table->string('amount')->default(0);
            $table->string('denomination')->nullable();
            $table->string('tax_option')->nullable();
            $table->string('tax')->default(0);
            $table->string('total')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_order_details');
    }
}
