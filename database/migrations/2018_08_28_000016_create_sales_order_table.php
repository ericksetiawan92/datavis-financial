<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sales_id')->nullable()->unsigned();
            $table->integer('customer_id')->nullable()->unsigned();
            $table->string('po_number');
            $table->string('so_number')->unique();
            $table->datetime('date')->nullable();
            $table->datetime('deadline')->nullable();
            $table->string('terms')->nullable();
            $table->boolean('include_tax')->nullable();
            $table->string('status');
            $table->string('tax_total')->default(0);
            $table->string('price_total')->default(0);
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_orders');
    }
}
