<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBigDenominationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('storages', function (Blueprint $table) {
            $table->dropColumn('big_denomination');
        });

        Schema::create('storage_big_denominations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('storage_id')->nullable()->unsigned();
            $table->text('name')->nullable();
            $table->text('value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('storages', function (Blueprint $table) {
            $table->string('big_denomination')->nullable();
        });

        Schema::dropIfExists('big_denomination');
    }
}
