<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Storage;
use App\Customer;
use App\Sales;
use App\SalesOrder;
use App\SalesOrderDetail;
use Illuminate\Http\Request;

class SalesOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $salesOrders = SalesOrder::orderBy('id', 'DESC')->get();
        $deletedSalesOrders = SalesOrder::onlyTrashed()->get();
        
        return view('pages.sales_order.index', [
            'salesOrders' => $salesOrders,
            'deletedSalesOrders' => $deletedSalesOrders,
            'page' => 'sales-order'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::orderBy('name', 'ASC')->get();
        $sales = Sales::orderBy('name', 'ASC')->get();
        $items = Storage::orderBy('name', 'ASC')->get();

        foreach ($items as $item) {
            $item->big_denomination_1 = (isset($item->bigDenominations[0])) ? $item->bigDenominations[0]->name : null;
            $item->big_denomination_2 = (isset($item->bigDenominations[1])) ? $item->bigDenominations[1]->name : null;
        }

        return view('pages.sales_order.form', [
            'customers' => $customers,
            'sales' => $sales,
            'items' => $items,
            'page' => 'sales-order'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $sales = SalesOrder::where('so_number', strtoupper($request->input('so_number')))->first();
            if (!$sales) {
                \DB::transaction(function () use ($request) {
                    $this->createRecord($request);
                });

                $alert = 'alert-success';
                $message = 'Sales Order added successful!';
            } else {
                $alert = 'alert-danger';
                $message = 'Sales Order Number already exist!';
            }
        } catch(\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Sales Order added failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('sales-orders');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $salesOrder = SalesOrder::withTrashed()->where('id', $id)->first();
        $customers = Customer::orderBy('name', 'ASC')->get();
        $sales = Sales::orderBy('name', 'ASC')->get();
        $items = Storage::orderBy('name', 'ASC')->get();

        foreach ($items as $item) {
            $item->big_denomination_1 = (isset($item->bigDenominations[0])) ? $item->bigDenominations[0]->name : null;
            $item->big_denomination_2 = (isset($item->bigDenominations[1])) ? $item->bigDenominations[1]->name : null;
        }

        return view('pages.sales_order.form_edit', [
            'salesOrder' => $salesOrder,
            'countSalesOrderDetails' => count($salesOrder->salesOrderDetails),
            'customers' => $customers,
            'sales' => $sales,
            'items' => $items,
            'page' => 'sales-order',
            'content' => 'show'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $salesOrder = SalesOrder::find($id);
        $customers = Customer::orderBy('name', 'ASC')->get();
        $sales = Sales::orderBy('name', 'ASC')->get();
        $items = Storage::orderBy('name', 'ASC')->get();

        foreach ($items as $item) {
            $item->big_denomination_1 = (isset($item->bigDenominations[0])) ? $item->bigDenominations[0]->name : null;
            $item->big_denomination_2 = (isset($item->bigDenominations[1])) ? $item->bigDenominations[1]->name : null;
        }

        return view('pages.sales_order.form_edit', [
            'salesOrder' => $salesOrder,
            'countSalesOrderDetails' => count($salesOrder->salesOrderDetails),
            'customers' => $customers,
            'sales' => $sales,
            'items' => $items,
            'page' => 'sales-order'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // try {
            $salesOrder = SalesOrder::where('so_number', strtoupper($request->input('so_number')))->where('id', '<>', $id)->first();
            if (!$salesOrder) {
                \DB::transaction(function () use ($request, $id) {
                    $this->updateRecord($request, $id);
                });
            
                $alert = 'alert-success';
                $message = 'Sales Order updated successful!';
            } else {
                $alert = 'alert-danger';
                $message = 'Sales Order Number already exist!';
            }
        // } catch(\Exception $e) {
        //     $alert = 'alert-danger';
        //     $message = 'Sales Order Number updated failed!';
        // }

        $request->session()->flash($alert, $message);
        return redirect('sales-orders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            \DB::transaction(function () use ($id) {
                $salesOrder = SalesOrder::find($id);
                $salesOrder->delete();
            });

            $alert = 'alert-success';
            $message = 'Sales Order deleted successful!';
        } catch(\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Sales Order deleted failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('sales-orders');
    }

    // ----- PRIVATE FUNCTION ---- //

    private function createRecord($request)
    {
        $salesOrder = SalesOrder::create([
            'po_number' => strtoupper($request->input('po_number')),
            'so_number' => strtoupper($request->input('so_number')),
            'date' => Carbon::parse($request->input('date'))->format('Y-m-d'),
            'deadline' => Carbon::parse($request->input('dealdine'))->format('Y-m-d'),
            'customer_id' => $request->input('customer_id'),
            'sales_id' => $request->input('sales_id'),
            'terms' => $request->input('term'),
            'include_tax' => ($request->input('include_tax')) ? true : false,
            'status' => SalesOrder::STATUS_PROCESS
        ]);

        $value = $this->createSalesOrderDetails($salesOrder, $request);

        $salesOrder->tax_total =  $value['tax'];
        $salesOrder->price_total =  $value['total'];
        $salesOrder->save();

        return true;
    }

    private function updateRecord($request, $id)
    {
        $salesOrder = SalesOrder::find($id);
        $salesOrder->update([
            'po_number' => strtoupper($request->input('po_number')),
            'so_number' => strtoupper($request->input('so_number')),
            'date' => Carbon::parse($request->input('date'))->format('Y-m-d'),
            'deadline' => Carbon::parse($request->input('dealdine'))->format('Y-m-d'),
            'customer_id' => $request->input('customer_id'),
            'sales_id' => $request->input('sales_id'),
            'terms' => $request->input('term'),
            'include_tax' => ($request->input('include_tax')) ? true : false,
            'status' => SalesOrder::STATUS_PROCESS
        ]);

        $this->deleteSalesOrderDetails($salesOrder);
        $value = $this->createSalesOrderDetails($salesOrder, $request);

        $salesOrder->tax_total =  $value['tax'];
        $salesOrder->price_total =  $value['total'];
        $salesOrder->save();

        return true;
    }

    private function deleteSalesOrderDetails($salesOrder)
    {
        foreach ($salesOrder->salesOrderDetails as $detail) {
            $detail->delete();
        }

        return true;
    }

    private function createSalesOrderDetails($salesOrder, $request)
    {
        $tax = 0;
        $total = 0;
        foreach ($request->input('item_ids') as $index => $itemId) {
            SalesOrderDetail::create([
                'sales_order_id' => $salesOrder->id,
                'storage_id' => (is_numeric($itemId)) ? $itemId : null,
                'storage_name' => (!is_numeric($itemId)) ? $itemId : null,
                'price' => $request->input('item_prices')[$index],
                'amount' => $request->input('item_amounts')[$index],
                'denomination' => (isset($request->input('item_denominations')[$index])) ? $request->input('item_denominations')[$index] : null,
                'tax_option' => $request->input('item_tax_options')[$index],
                'tax' => $request->input('item_taxes')[$index],
                'total' => $request->input('item_totals')[$index]
            ]);

            $tax += $request->input('item_taxes')[$index];
            $total += $request->input('item_totals')[$index];
        }

        return [
            'tax' => $tax,
            'total' => $total   
        ];
    }
}
