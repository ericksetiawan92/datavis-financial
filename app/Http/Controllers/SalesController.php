<?php

namespace App\Http\Controllers;

use App\Sales;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = Sales::all();
        $deletedSales = Sales::onlyTrashed()->get();
        
        return view('pages.sales.index', [
            'sales' => $sales,
            'deletedSales' => $deletedSales,
            'page' => 'sales'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.sales.form', [
            'page' => 'sales'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $sales = Sales::where('sales_id', strtoupper($request->input('sales_id')))->first();
            if (!$sales) {
                \DB::transaction(function () use ($request) {
                    $this->createRecord($request);
                });

                $alert = 'alert-success';
                $message = 'Sales added successful!';
            } else {
                $alert = 'alert-danger';
                $message = 'Sales ID already exist!';
            }
        } catch(\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Sales added failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('sales');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sales = Sales::withTrashed()->where('id', $id)->first();

        return view('pages.sales.form', [
            'sales' => $sales,
            'content' => 'show',
            'page' => 'sales'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sales = Sales::find($id);

        return view('pages.sales.form', [
            'sales' => $sales,
            'page' => 'sales'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $sales = Sales::where('sales_id', strtoupper($request->input('sales_id')))->where('id', '<>', $id)->first();
            if (!$sales) {
                \DB::transaction(function () use ($request, $id) {
                    $this->updateRecord($request, $id);
                });
            
                $alert = 'alert-success';
                $message = 'Sales updated successful!';
            } else {
                $alert = 'alert-danger';
                $message = 'Sales ID already exist!';
            }
        } catch(\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Sales updated failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('sales');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            \DB::transaction(function () use ($id) {
                $sales = Sales::find($id);
                $sales->delete();
            });

            $alert = 'alert-success';
            $message = 'Sales deleted successful!';
        } catch(\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Sales deleted failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('sales');
    }

    /**
     * Download excel bulk import file
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download()
    {
        $filename = 'sales-bulk-import';
        $header = ['Sales ID *', 'Name *', 'Jobtitle *'];
        $columnFormat = ['A' => '@', 'B' => '@', 'C' => '@'];

        Excel::create($filename, function ($excel) use ($header, $columnFormat) {
            $excel->sheet('Sheet1', function ($sheet) use ($header, $columnFormat) {
                $sheet->setColumnFormat($columnFormat);
                $sheet->row(1, $header);
            });
        })->download('xls');
    }

    /**
     * Upload excel bulk import file
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        $data  = [];
        $input = $request->file('file');
        
        Excel::selectSheets('Sheet1')->load($input, function($reader) use (&$data) {
            $reader->skip(1);
            $reader->noHeading();
            $reader->select()->each(function ($row) use(&$data) {
                $array = $row->toArray();

                if ($array[0] && $array[1] && $array[2]) 
                    $data[] = $row->toArray();
            });
        });

        foreach ($data as $value) {
            try {
                $sales = Sales::where('sales_id', strtoupper($value[0]))->first();
                if (!$sales) {
                    \DB::transaction(function () use ($value) {
                        $this->createRecord($value);
                    });
    
                    $alert = 'alert-success';
                    $message = 'Sales added successful!';
                } else {
                    $alert = 'alert-danger';
                    $message = 'Sales ID ' . $value[0] . ' already exist!';
                    break;
                }
            } catch(\Exception $e) {
                $alert = 'alert-danger';
                $message = 'Sales added failed!';
                break;
            }
        }

        $request->session()->flash($alert, $message);
        return redirect('sales');
    }

    // ----- PRIVATE FUNCTION ---- //

    private function createRecord($request)
    {
        if (is_array($request)) {
            Sales::create([
                'sales_id' => $request[0],
                'name' => $request[1],
                'jobtitle' => $request[2]
            ]);
        } else {
            Sales::create([
                'sales_id' => strtoupper($request->input('sales_id')),
                'name' => $request->input('name'),
                'jobtitle' => $request->input('jobtitle')
            ]);
        }

        return true;
    }

    private function updateRecord($request, $id)
    {
        $sales = Sales::find($id);
        $sales->update([
            'sales_id' => strtoupper($request->input('sales_id')),
            'name' => $request->input('name'),
            'jobtitle' => $request->input('jobtitle')
        ]);

        return true;
    }
}
