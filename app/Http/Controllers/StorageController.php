<?php

namespace App\Http\Controllers;

use App\Storage;
use App\StorageBigDenomination;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class StorageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $storages = Storage::all();
        $deletedStorages = Storage::onlyTrashed()->get();

        return view('pages.storage.index', [
            'storages' => $storages,
            'deletedStorages' => $deletedStorages,
            'page' => 'storage'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.storage.form', [
            'page' => 'storage'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $storage = Storage::where('storage_id', strtoupper($request->input('storage_id')))->first();
            if (!$storage) {
                \DB::transaction(function () use ($request) {
                    $storage = $this->createStorage($request);

                    $this->createBigDenomination($storage, $request);
                });

                $alert = 'alert-success';
                $message = 'Storage added successful!';
            } else {
                $alert = 'alert-danger';
                $message = 'Storage ID already exist!';
            }
        } catch(\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Storage added failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('storages');
    }

    /**
     * Show the detail.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $storage = Storage::onlyTrashed()->where('id', $id)->first();

        $name = [0 => null, 1 => null];
        $value = [0 => null, 1 => null];
        $storageBigDenominations = $storage->bigDenominations;
        foreach ($storageBigDenominations as $index => $bigDenomination) {
            $name[$index] = $bigDenomination->name;
            $value[$index] = $bigDenomination->value;
        }

        return view('pages.storage.form', [
            'storage' => $storage,
            'nameBigDenominations' => $name,
            'valueBigDenominations' => $value,
            'content' => 'show',
            'page' => 'storage'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $storage = Storage::find($id);

        $name = [0 => null, 1 => null];
        $value = [0 => null, 1 => null];
        $storageBigDenominations = $storage->bigDenominations;
        foreach ($storageBigDenominations as $index => $bigDenomination) {
            $name[$index] = $bigDenomination->name;
            $value[$index] = $bigDenomination->value;
        }

        return view('pages.storage.form', [
            'storage' => $storage,
            'nameBigDenominations' => $name,
            'valueBigDenominations' => $value,
            'page' => 'storage'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $storage = Storage::where('storage_id', strtoupper($request->input('storage_id')))->where('id', '<>', $id)->first();
            if (!$storage) {
                \DB::transaction(function () use ($request, $id) {
                    $storage = Storage::find($id);
                    
                    $this->updateStorage($storage, $request);
                    $this->deleteBigDenomination($storage);
                    $this->createBigDenomination($storage, $request);
                });

                $alert = 'alert-success';
                $message = 'Storage updated successful!';
            } else {
                $alert = 'alert-danger';
                $message = 'Storage ID already exist!';
            }
        } catch(\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Storage updated failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('storages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            \DB::transaction(function () use ($id) {
                $storage = Storage::find($id);
                $storage->delete();
            });

            $alert = 'alert-success';
            $message = 'Storage deleted successful!';
        } catch(\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Storage deleted failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('storages');
    }

    /**
     * Download excel bulk import file
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download()
    {
        $filename = 'storages-bulk-import';
        $header = ['Storage ID *', 'Name *', 'Small Denomination', 'Big Denomination 1 Name', 'Big Denomination 1 Value', 'Big Denomination 2 Name', 'Big Denomination 2 Value'];
        $columnFormat = ['A' => '@', 'B' => '@', 'C' => '@', 'D' => '@', 'E' => '@', 'F' => '@', 'G' => '@'];

        Excel::create($filename, function ($excel) use ($header, $columnFormat) {
            $excel->sheet('Sheet1', function ($sheet) use ($header, $columnFormat) {
                $sheet->setColumnFormat($columnFormat);
                $sheet->row(1, $header);
            });
        })->download('xls');
    }

    /**
     * Upload excel bulk import file
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        $data  = [];
        $input = $request->file('file');
        
        Excel::selectSheets('Sheet1')->load($input, function($reader) use (&$data) {
            $reader->skip(1);
            $reader->noHeading();
            $reader->select()->each(function ($row) use(&$data) {
                $array = $row->toArray();

                if ($array[0] && $array[1]) 
                    $data[] = $row->toArray();
            });
        });

        foreach ($data as $value) {
            try {
                $storage = Storage::where('storage_id', strtoupper($value[0]))->first();
                if (!$storage) {
                    \DB::transaction(function () use ($value) {
                        $storage = $this->createStorage($value);

                        if ($value[2]) {
                            $this->createBigDenomination($storage, $value);
                        }
                    });
    
                    $alert = 'alert-success';
                    $message = 'Customer added successful!';
                } else {
                    $alert = 'alert-danger';
                    $message = 'Customer ID ' . $value[0] . ' already exist!';
                    break;
                }
            } catch(\Exception $e) {
                $alert = 'alert-danger';
                $message = 'Customer added failed!';
                break;
            }
        }

        $request->session()->flash($alert, $message);
        return redirect('storages');
    }



    // ----- PRIVATE FUNCTION ---- //

    private function createStorage($request)
    {
        if (is_array($request)) {
            $storage = Storage::create([
                'storage_id' => $request[0],
                'name' => $request[1],
                'small_denomination' => $request[2]
            ]);
        } else {
            $storage = Storage::create([
                'storage_id' => $request->input('storage_id'),
                'name' => $request->input('name'),
                'small_denomination' => $request->input('small_denomination')
            ]);
        }

        return $storage;
    }

    private function createBigDenomination($storage, $request)
    {
        if (is_array($request)) {
            $indexName = 1;
            $indexValue = 2;
            $counter = 0;
            for ($i=1; $i<=2; $i++) {
                $counter = $counter + 2;;
                if (!$request[$indexName+$counter]) continue;
                
                StorageBigDenomination::create([
                    'storage_id' => $storage->id,
                    'name' => $request[$indexName+$counter],
                    'value' => ($request[$indexValue+$counter]) ? $request[$indexValue+$counter] : 0
                ]);
            }
        } else {
            foreach ($request['big_denomination'] as $index => $value) {
                if (!$value) continue;

                StorageBigDenomination::create([
                    'storage_id' => $storage->id,
                    'name' => $value,
                    'value' => ($request->input('value_big_denomination'))[$index] ? $request->input('value_big_denomination')[$index] : 0
                ]);
            }
        }

        return true;
    }

    private function updateStorage($storage, $request)
    {
        $storage->update([
            'storage_id' => $request->input('storage_id'),
            'name' => $request->input('name'),
            'small_denomination' => $request->input('small_denomination')
        ]);

        return true;
    }

    private function deleteBigDenomination($storage)
    {
        $bigDenominations = $storage->bigDenominations;
        foreach ($bigDenominations as $bigDenomination) {
            $bigDenomination->delete();
        }

        return true;
    }
}
