<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::all();
        $deletedCustomers = Customer::onlyTrashed()->get();
        
        return view('pages.customer.index', [
            'customers' => $customers,
            'deletedCustomers' => $deletedCustomers,
            'page' => 'customer'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.customer.form', [
            'page' => 'customer'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $customer = Customer::where('customer_id', strtoupper($request->input('customer_id')))->first();
            if (!$customer) {
                \DB::transaction(function () use ($request) {
                    $this->createRecord($request);
                });

                $alert = 'alert-success';
                $message = 'Customer added successful!';
            } else {
                $alert = 'alert-danger';
                $message = 'Customer ID already exist!';
            }
        } catch(\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Customer added failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('customers');
    }

    /**
     * Show the detail of record.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::withTrashed()->where('id', $id)->first();

        return view('pages.customer.form', [
            'customer' => $customer,
            'content' => 'show',
            'page' => 'customer'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);

        return view('pages.customer.form', [
            'customer' => $customer,
            'page' => 'customer'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $customer = Customer::where('customer_id', strtoupper($request->input('customer_id')))->where('id', '<>', $id)->first();
            if (!$customer) {
                \DB::transaction(function () use ($request, $id) {
                    $this->updateRecord($request, $id);
                });
            
                $alert = 'alert-success';
                $message = 'Customer updated successful!';
            } else {
                $alert = 'alert-danger';
                $message = 'Customer ID already exist!';
            }
        } catch(\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Customer updated failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('customers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            \DB::transaction(function () use ($id) {
                $customer = Customer::find($id);
                $customer->delete();
            });

            $alert = 'alert-success';
            $message = 'Customer deleted successful!';
        } catch(\Exception $e) {
            $alert = 'alert-danger';
            $message = 'Customer deleted failed!';
        }

        $request->session()->flash($alert, $message);
        return redirect('customers');
    }

    /**
     * Download excel bulk import file
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download()
    {
        $filename = 'customers-bulk-import';
        $header = ['Customer ID *', 'Name *', 'Email', 'Phone Office *', 'Contact Person *', 'Address *', 'PIC Finance', 'Finance Phone', 'City', 'Province', 'Postal Code', 'Website'];
        $columnFormat = [
            'A' => '@', 'B' => '@', 'C' => '@', 'D' => '@', 'E' => '@', 'F' => '@', 'G' => '@', 'H' => '@', 
            'I' => '@', 'J' => '@', 'K' => '@', 'L' => '@'
        ];

        Excel::create($filename, function ($excel) use ($header, $columnFormat) {
            $excel->sheet('Sheet1', function ($sheet) use ($header, $columnFormat) {
                $sheet->setColumnFormat($columnFormat);
                $sheet->row(1, $header);
            });
        })->download('xls');
    }

    /**
     * Upload excel bulk import file
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        $data  = [];
        $input = $request->file('file');
        
        Excel::selectSheets('Sheet1')->load($input, function($reader) use (&$data) {
            $reader->skip(1);
            $reader->noHeading();
            $reader->select()->each(function ($row) use(&$data) {
                $array = $row->toArray();

                if ($array[0] && $array[1] && $array[3] && $array[4] && $array[5]) 
                    $data[] = $row->toArray();
            });
        });

        foreach ($data as $value) {
            try {
                $customer = Customer::where('customer_id', strtoupper($value[0]))->first();
                if (!$customer) {
                    \DB::transaction(function () use ($value) {
                        $this->createRecord($value);
                    });
    
                    $alert = 'alert-success';
                    $message = 'Customer added successful!';
                } else {
                    $alert = 'alert-danger';
                    $message = 'Customer ID ' . $value[0] . ' already exist!';
                    break;
                }
            } catch(\Exception $e) {
                $alert = 'alert-danger';
                $message = 'Customer added failed!';
                break;
            }
        }

        $request->session()->flash($alert, $message);
        return redirect('customers');
    }



    // ----- PRIVATE FUNCTION ---- //

    private function createRecord($request)
    {
        if (is_array($request)) {
            Customer::create([
                'customer_id' => strtoupper($request[0]),
                'name' => $request[1],
                'email' => $request[2],
                'phone_office' => $request[3],
                'contact_person' => $request[4],
                'address' => $request[5],
                'pic_finance' => $request[6],
                'finance_phone' => $request[7],
                'city' => strtoupper($request[8]),
                'province' => strtoupper($request[9]),
                'postal_code' => $request[10],
                'website' => $request[11]
            ]);
        } else {
            Customer::create([
                'customer_id' => strtoupper($request->input('customer_id')),
                'name' => $request->input('name'),
                'phone_office' => $request->input('phone_office'),
                'contact_person' => $request->input('contact_person'),
                'address' => $request->input('address'),
                'pic_finance' => $request->input('pic_finance'),
                'finance_phone' => $request->input('finance_phone'),
                'city' => strtoupper($request->input('city')),
                'province' => strtoupper($request->input('province')),
                'postal_code' => $request->input('postal_code'),
                'email' => $request->input('email'),
                'website' => $request->input('website')
            ]);
        }

        return true;
    }
    
    private function updateRecord($request, $id)
    {
        $customer = Customer::find($id);
        $customer->update([
            'customer_id' => $request->input('customer_id'),
            'name' => $request->input('name'),
            'phone_office' => $request->input('phone_office'),
            'contact_person' => $request->input('contact_person'),
            'address' => $request->input('address'),
            'pic_finance' => $request->input('pic_finance'),
            'finance_phone' => $request->input('finance_phone'),
            'city' => strtoupper($request->input('city')),
            'province' => strtoupper($request->input('province')),
            'postal_code' => $request->input('postal_code'),
            'email' => $request->input('email'),
            'website' => $request->input('website')
        ]);

        return true;
    }
}
