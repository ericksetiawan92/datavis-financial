<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOrderDetail extends Model
{
    protected $table = 'sales_order_details';
    protected $guarded = [];

    public $timestamps = false;

    public function storage()
  	{
  		return $this->belongsTo('App\Storage');
    }
      
    public function salesOrder()
  	{
  		return $this->belongsTo('App\SalesOrder');
  	}
}
