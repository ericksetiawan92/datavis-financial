<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StorageBigDenomination extends Model
{
    protected $table = 'storage_big_denominations';
    protected $guarded = [];

    public $timestamps = false;

    public function storage()
    {
    	return $this->belongsTo('App\Storage');
    }
}
