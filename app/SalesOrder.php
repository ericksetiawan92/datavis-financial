<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesOrder extends Model
{
    const STATUS_PROCESS = 'PROCESS';
    
    use SoftDeletes;
    
    protected $table = 'sales_orders';
    protected $guarded = [];
    
    public $timestamps = true;

    public function customer()
  	{
  		return $this->belongsTo('App\Customer');
    }
      
    public function sales()
  	{
  		return $this->belongsTo('App\Sales');
    }
    
    public function salesOrderDetails()
  	{
  		return $this->hasMany('App\SalesOrderDetail');
  	}
}
