<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sales extends Model
{
    use SoftDeletes;
    
    protected $table = 'sales';
    protected $guarded = [];
    
    public $timestamps = true;

    public function salesOrders()
  	{
  		return $this->hasMany('App\SalesOrder');
  	}
}
