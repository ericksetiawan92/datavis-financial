<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Storage extends Model
{
    use SoftDeletes;
    
    protected $table = 'storages';
    protected $guarded = [];
    
    public $timestamps = true;

    public function bigDenominations()
    {
    	return $this->hasMany('App\StorageBigDenomination');
    }
}
